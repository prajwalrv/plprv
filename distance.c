#include <stdio.h>
#include <math.h>

int main()
{
    float a,b,c,d,dist;
    printf("Enter the 1st point:\n");
    scanf("%f%f",&a,&b);
    printf("Enter the 2nd point:\n");
    scanf("%f%f",&c,&d);
    dist=sqrt(pow(c-a,2)+pow(d-b,2));
    printf("The distance between the two points is %f\n",dist);
    return 0;
}
